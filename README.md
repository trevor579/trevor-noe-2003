# Beanbook #

An app for Android

## About ##

* Beanbook is a niche recipe app.
* Recipes must contain beans as an ingredient.
* Recipe list is user generated.
* Users must create an account to add their own recipes.

## Prototype ##

[View the app prototype](https://xd.adobe.com/view/8c068996-3061-41d3-5f60-33e2ad53ce5d-fa37/) made using Adobe XD

---

## Installation ##

#### Using an Android Device ####
1. In settings on your Android device, go to Security and tap "Unknown Sources"
2. Download the APK to your device.
3. Go to the folder that it was downloaded to.
4. Tap the APK to install

#### Using Android Studio ####
1. [Download Android Studio](https://developer.android.com/studio) if you don't already have it
2. Open the "Beanbook" folder in Android Studio
3. Set up an Android Virtual Device, or plug an Android device into your computer
4. Press the play button at the top of the window.

---

## Known Bugs ##
 * None at this time

## Hardware Requirements ##
 
  * A way to run APK files. This can include either: 
	- An Android device running Android 6.0 or later
	- A Windows device or Mac with Android Studio and an emulator set up.
 * An internet connection
