package com.example.beanbook.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.fragments.RecipeDetailsFragment;

public class RecipeDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);

        User currUser;

        // Handle intent
        Intent intent = getIntent();
        Recipe currRecipe = (Recipe) intent.getSerializableExtra("currRecipe");
        Recipe oldRecipe = (Recipe) intent.getSerializableExtra("oldRecipe");
        Boolean isPreview = intent.getBooleanExtra("isPreview", false);
        currUser = (User) intent.getSerializableExtra("user");



        // Set the action bar title according to what user is doing on screen
        if (getSupportActionBar() != null) {
            if (isPreview) {
                getSupportActionBar().setTitle("Preview Recipe");
            } else {

                if (currRecipe != null)
                getSupportActionBar().setTitle(currRecipe.getTitle());
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.FragmentContainer, RecipeDetailsFragment.newInstance(currRecipe, oldRecipe, isPreview, currUser))
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
