package com.example.beanbook.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.RecipeDetailsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;

import java.util.ArrayList;

public class DirectionsFormFragment extends Fragment implements View.OnClickListener{

    private ArrayAdapter<String> adapter;
    private ArrayList<String> directions;

    public static DirectionsFormFragment newInstance(String title, String time, String price,
                                                     String beanType, ArrayList<String> ingredients,
                                                     User currUser, ArrayList<String> tags,Recipe recipe) {

        Bundle args = new Bundle();

        args.putString("title", title);
        args.putString("time", time);
        args.putString("price", price);
        args.putString("beanType", beanType);
        args.putStringArrayList("ingredients", ingredients);
        args.putSerializable("user", currUser);
        args.putSerializable("tags", tags);
        args.putSerializable("recipe", recipe);

        DirectionsFormFragment fragment = new DirectionsFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.directions_fragment, container, false);

        Button addButton = view.findViewById(R.id.add_direction_button);
        addButton.setOnClickListener(this);

        directions = new ArrayList<>();

        if (getContext() != null)
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, directions);

        final ListView directionsList = view.findViewById(R.id.directions_list);
        directionsList.setAdapter(adapter);

        directionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                // Open dialog box to let user edit the selected direction
                AlertDialog.Builder editBuilder = new AlertDialog.Builder(getContext());

                editBuilder.setTitle("Edit Direction");

                final EditText input = new EditText(getContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(8, 8, 8, 8);
                input.setLayoutParams(lp);

                input.setText(directions.get(position));

                editBuilder.setView(input);

                editBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (input.getText().toString().isEmpty()) {
                            Toast.makeText(getContext(),
                                    getResources().getString(R.string.must_fill_1_ingredient),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            directions.remove(position);
                            directions.add(position, input.getText().toString());
                            adapter.notifyDataSetChanged();
                        }
                    }
                });

                editBuilder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        directions.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                editBuilder.setNeutralButton("Cancel", null);
                editBuilder.show();
            }
        });

        if (getArguments() != null) {
            Recipe recipe = (Recipe) getArguments().getSerializable("recipe");

            if (recipe != null) {
                for (String direction : recipe.getDirections()) {
                    addDirection(direction, view);
                }
            }
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.preview_item, menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.preview_menu_item) {

            if (directions.isEmpty()) {
                Toast.makeText(getContext(), getResources().getString(R.string.enter_one_direction), Toast.LENGTH_SHORT).show();
                return false;
            }

            if (getArguments() == null) {
                Toast.makeText(getContext(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
            }

            String title = getArguments().getString("title");
            String time = getArguments().getString("time");
            String price = getArguments().getString("price");
            String beanType = getArguments().getString("beanType");
            ArrayList<String> ingredients = getArguments().getStringArrayList("ingredients");
            User currUser = (User) getArguments().getSerializable("user");
            ArrayList<String> tags = getArguments().getStringArrayList("tags");
            Recipe oldRecipe = (Recipe) getArguments().getSerializable("recipe");

            Recipe currRecipe = null;

            if (currUser != null) {
                currRecipe = new Recipe(title, currUser.getUsername(), price, time, beanType, ingredients, directions, tags);
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.not_logged_in), Toast.LENGTH_SHORT).show();
            }


            // Go to preview
            Intent previewIntent = new Intent(getContext(), RecipeDetailsActivity.class);
            previewIntent.putExtra("currRecipe", currRecipe);
            previewIntent.putExtra("oldRecipe", oldRecipe);
            previewIntent.putExtra("isPreview", true);
            previewIntent.putExtra("user", currUser);
            startActivity(previewIntent);

            return true;
        }
        return false;
    }

    private void addDirection(String newDirection, View view) {
        if (view != null) {
            directions.add(newDirection);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        if (getView() != null) {
            EditText directionEditText = getView().findViewById(R.id.direction_edit_text);


            if (directionEditText.getText().toString().isEmpty()) {
                Toast.makeText(getContext(), "No direction entered!", Toast.LENGTH_SHORT).show();
                return;
            }

            String newDirection = directionEditText.getText().toString();

            addDirection(newDirection, getView());
            directionEditText.setText("");

        }
    }
}
