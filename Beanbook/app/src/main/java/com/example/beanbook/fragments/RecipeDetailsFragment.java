package com.example.beanbook.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.MainActivity;
import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RecipeDetailsFragment extends Fragment {

    private DatabaseReference recipes;

    private Recipe currRecipe;

    public static RecipeDetailsFragment newInstance(Recipe currRecipe, Recipe oldRecipe, Boolean isPreview, User currUser) {

        Bundle args = new Bundle();
        args.putSerializable("currRecipe", currRecipe);
        args.putSerializable("oldRecipe", oldRecipe);
        args.putBoolean("isPreview", isPreview);
        args.putSerializable("user", currUser);

        RecipeDetailsFragment fragment = new RecipeDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recipe_details_fragment, container, false);

        if (getArguments() != null) {
            currRecipe = (Recipe) getArguments().getSerializable("currRecipe");

            TextView tv = view.findViewById(R.id.title_text);
            tv.setText(currRecipe.getTitle());

            tv = view.findViewById(R.id.author_text);
            tv.setText(currRecipe.getAuthor());

            tv = view.findViewById(R.id.tags_text);
            if (currRecipe.getTags() != null) {
                StringBuilder builder = new StringBuilder();
                if (currRecipe.getTags().size() > 1) {
                    int counter = 0;
                    for (String tag : currRecipe.getTags()) {
                        builder.append(tag);
                        if (counter + 1 != currRecipe.getTags().size())
                            builder.append(", ");

                        counter++;
                    }
                } else if (currRecipe.getTags().size() != 0){
                    builder.append(currRecipe.getTags().get(0));
                }
                tv.setText(builder.toString());
            }

            tv = view.findViewById(R.id.time_text);
            String time = "Time: " + currRecipe.getCookTime() + " minutes";
            tv.setText(time);

            tv = view.findViewById(R.id.price_text);
            String price = "Price: $" + currRecipe.getPrice();
            tv.setText(price);

            tv = view.findViewById(R.id.ingredients_text);
            tv.setText(ingredientString(currRecipe.getIngredients()));

            tv = view.findViewById(R.id.directions_text);
            tv.setText(directionString(currRecipe.getDirections()));
        }

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recipes = FirebaseDatabase.getInstance().getReference("Recipes");

        // only have the options menu if this is a preview
        if (getArguments() != null) {
            if (getArguments().getBoolean("isPreview")) {
                setHasOptionsMenu(true);
            }
        }
    }


    // Format ArrayLists
    private String ingredientString(ArrayList<String> ingredients) {
        StringBuilder ingredientBuilder = new StringBuilder();

        for (String ingredient : ingredients) {
            ingredientBuilder.append(" - ");
            ingredientBuilder.append(ingredient);
            ingredientBuilder.append("\n\n");
        }

        return ingredientBuilder.toString();
    }

    private String directionString(ArrayList<String> directions) {

        StringBuilder directionBuilder = new StringBuilder();
        int counter = 0;

        for (String direction : directions) {
            counter++;
            directionBuilder.append(counter);
            directionBuilder.append(". ");
            directionBuilder.append(direction);
            directionBuilder.append("\n\n");
        }

        return directionBuilder.toString();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_item, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.save_item) {

            // characters that cannot be in title
            String titleRegex = "[.#$]";
            final String currRecipeTitle = currRecipe.getTitle().replaceAll(titleRegex, "");
            // Save recipe to database
            recipes.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    // if editing, make sure to update rather than save as new recipe
                    if (getArguments() != null) {
                        Recipe oldRecipe = (Recipe) getArguments().getSerializable("oldRecipe");
                        if (oldRecipe != null) { // user is editing

                            // remove the old recipe
                            //check if old naming convention is used
                            if (dataSnapshot.child(oldRecipe.getTitle()).exists()) {
                                recipes.child(oldRecipe.getTitle()).removeValue();

                            } else { // otherwise use new one

                                recipes.child(oldRecipe.getTitle() + oldRecipe.getAuthor()).removeValue();
                            }

                            // replace with new recipe
                            recipes.child(currRecipeTitle + currRecipe.getAuthor()).setValue(currRecipe);
                        } else { // user is adding

                            // check if name exists already, if it does, add number to the end
                            if (dataSnapshot.child(currRecipeTitle + currRecipe.getAuthor()).exists()) {
                                recipes.child(currRecipeTitle + currRecipe.getAuthor() + "(1)").setValue(currRecipe);
                            } else {
                                recipes.child(currRecipeTitle + currRecipe.getAuthor()).setValue(currRecipe);
                            }
                        }
                    } else {
                        Toast.makeText(getContext(),
                                getResources().getString(R.string.problem_adding_recipe),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(getContext(), getResources().getString(R.string.recipe_saved), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getContext(),
                            "Problem saving recipe. Try again later.",
                            Toast.LENGTH_SHORT).show();
                }
            });

            User currUser = null;
            if (getArguments() != null) {
                currUser = (User) getArguments().getSerializable("user");
            }

            // Go to recipe list

            Intent listIntent = new Intent(getContext(), MainActivity.class);
            listIntent.putExtra("user", currUser);

            // make sure user can't press back button to re-enter editing
            listIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(listIntent);

            if (getActivity() != null)
                getActivity().finish();

            return super.onOptionsItemSelected(item);

        }
        return false;
    }
}
