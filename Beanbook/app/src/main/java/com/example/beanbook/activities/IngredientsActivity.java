package com.example.beanbook.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.fragments.IngredientsFormFragment;

import java.util.ArrayList;

public class IngredientsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_ingredients);



        // Handle intent
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String time = intent.getStringExtra("time");
        String price = intent.getStringExtra("price");
        String beanType = intent.getStringExtra("beanType");
        ArrayList<String> tags = intent.getStringArrayListExtra("tags");
        User currUser = (User) intent.getSerializableExtra("user");
        Recipe editingRecipe = (Recipe) intent.getSerializableExtra("recipe");


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Add Ingredients");
            getSupportActionBar().setSubtitle("Tap ingredient to edit");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.FragmentContainer, IngredientsFormFragment.newInstance(title, time,
                        price, beanType, currUser, tags,editingRecipe))
                .commit();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
