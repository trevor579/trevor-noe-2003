package com.example.beanbook;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.beanbook.activities.SignInActivity;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.fragments.RecipeListFragment;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class MainActivity extends AppCompatActivity {

    public static final String fileName = "user";

    private User currUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Load saved user from device storage
        loadUser();

        Intent intent = getIntent();
        if (intent.hasExtra("user")) {
            currUser = (User) intent.getSerializableExtra("user");
        }

        // if the user hasn't signed in before, open the sign in screen
        if (currUser == null) {
            Intent signIn = new Intent(this, SignInActivity.class);
            startActivityForResult(signIn, 1);
        }

        if (currUser != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.FragmentContainer, RecipeListFragment.newInstance(currUser))
                    .commit();
        }
    }

    private void loadUser() {
        try {
            FileInputStream fis = openFileInput(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            currUser = (User) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            if (data != null) {
                currUser = (User) data.getSerializableExtra("user");

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.FragmentContainer, RecipeListFragment.newInstance(currUser))
                        .commit();
            }
        } else {

            // if user cancels sign in, close application
            finish();
        }
    }
}
