package com.example.beanbook.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.fragments.DetailsFormFragment;

public class DetailsFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_form);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Recipe Details");
            // set back button in action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // get the currUser
        Intent intent = getIntent();
        User currUser = (User) intent.getSerializableExtra("user");

        // Recipe extra is not required so check if exists, if not, set it to null
        Recipe recipe = null;
        if (intent.hasExtra("recipe"))
            recipe = (Recipe) intent.getSerializableExtra("recipe");

        getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, DetailsFormFragment.newInstance(currUser, recipe))
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
