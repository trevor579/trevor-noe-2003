package com.example.beanbook.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.MainActivity;
import com.example.beanbook.R;
import com.example.beanbook.dataModels.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.regex.Pattern;

public class CreateAccountFragment extends Fragment implements View.OnClickListener {

    private DatabaseReference users;

    public static CreateAccountFragment newInstance() {
        return new CreateAccountFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.create_account_fragment, container, false);

        Button addButton = view.findViewById(R.id.sign_up_button);
        addButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");
    }

    @Override
    public void onClick(View v) {

        View fragmentView = getView();
        if (fragmentView != null) {

            EditText emailET = fragmentView.findViewById(R.id.email_edit_text);
            String email = emailET.getText().toString();
            EditText passwordET = fragmentView.findViewById(R.id.password_edit_text);
            String password = passwordET.getText().toString();
            EditText usernameET = fragmentView.findViewById(R.id.username_edit_text);
            final String username = usernameET.getText().toString();

            // email validation from OWASP Validation Regex Repository
            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                    "[a-zA-Z0-9_+&*-]+)*@" +
                    "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                    "A-Z]{2,7}$";

            Pattern pat = Pattern.compile(emailRegex);


            if (email.isEmpty() || password.isEmpty() || username.isEmpty()) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_fields_blank), Toast.LENGTH_SHORT).show();
                return;
            } else if (!pat.matcher(email).matches()) {
                Toast.makeText(getContext(), getResources().getString(R.string.enter_valid_email), Toast.LENGTH_SHORT).show();
                return;
            } else if (username.contains(" ")) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_spaces), Toast.LENGTH_SHORT).show();
                return;
            }

            final User newUser = new User(username, email, password);

            // Add new user to database

            users.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.child(newUser.getUsername()).exists()) { // Username taken
                        Toast.makeText(getContext(), getResources().getString(R.string.username_taken), Toast.LENGTH_SHORT).show();

                    } else { // User input is valid
                        users.child(newUser.getUsername()).setValue(newUser);

                        Toast.makeText(getContext(),
                                "Account Creation Successful. Welcome " + newUser.getUsername() + "!",
                                Toast.LENGTH_SHORT).show();

                        // Save new user to device
                        saveUser(newUser);

                        if (getActivity() == null) {
                            return;
                        }

                        // Send user to recipe list and open
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("user", newUser);
                        getActivity().setResult(Activity.RESULT_OK, resultIntent);
                        getActivity().finish();

                    }
                }

                private void saveUser(User user) {

                    if (getContext() != null) {
                        try {
                            FileOutputStream fos = getContext().openFileOutput(MainActivity.fileName, Context.MODE_PRIVATE);
                            ObjectOutputStream os = new ObjectOutputStream(fos);
                            os.writeObject(user);
                            os.close();
                            fos.close();
                        } catch (IOException e) {
                            Toast.makeText(getContext(),
                                    R.string.login_fail_error_message,
                                    Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(),
                                R.string.login_fail_error_message,
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getContext(),
                            getResources().getString(R.string.problem_creating_account),
                            Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
