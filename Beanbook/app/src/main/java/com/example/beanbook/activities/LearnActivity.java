package com.example.beanbook.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.beanbook.R;
import com.example.beanbook.fragments.LearnFragment;

public class LearnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.learn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.FragmentContainer, LearnFragment.newInstance())
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
