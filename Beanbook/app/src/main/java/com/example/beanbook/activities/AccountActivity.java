package com.example.beanbook.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.fragments.AccountFragment;

import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        Intent intent = getIntent();
        User currUser = null;
        ArrayList<Recipe> recipes = null;

        if (intent.hasExtra("user") && intent.hasExtra("recipes")) {
            currUser = (User) intent.getSerializableExtra("user");
            recipes = (ArrayList<Recipe>) intent.getSerializableExtra("recipes");

            if (getSupportActionBar() != null && currUser != null) {
                getSupportActionBar().setTitle(currUser.getUsername());
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        } else {
            Toast.makeText(this, getString(R.string.problem_retrieving_account),
                    Toast.LENGTH_LONG).show();

            finish();
        }


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.FragmentContainer, AccountFragment.newInstance(currUser, recipes))
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
