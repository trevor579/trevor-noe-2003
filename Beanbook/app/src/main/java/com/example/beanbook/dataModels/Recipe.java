package com.example.beanbook.dataModels;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class Recipe implements Serializable {

    private String title;
    private String author;
    private String price;
    private String cookTime;
    private String beanType;
    private ArrayList<String> ingredients;
    private ArrayList<String> directions;
    private ArrayList<String> tags;

    public Recipe() { }

    public Recipe(String title, String author, String price, String cookTime, String beanType,
                  ArrayList<String> ingredients, ArrayList<String> directions,
                  ArrayList<String > tags) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.cookTime = cookTime;
        this.beanType = beanType;
        this.ingredients = ingredients;
        this.directions = directions;
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getPrice() {
        return price;
    }

    public String getCookTime() {
        return cookTime;
    }

    public String getBeanType() {
        return beanType;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public ArrayList<String> getDirections() {
        return directions;
    }

    public ArrayList<String> getTags() { return tags; }

    @NonNull
    @Override
    public String toString() {
        return title + " | By: " + author;
    }
}
