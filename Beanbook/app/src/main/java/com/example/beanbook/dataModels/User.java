package com.example.beanbook.dataModels;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private String email;
    private String password;


    public User() {

    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    @SuppressWarnings("unused") // This is used by Firebase to add to database
    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
