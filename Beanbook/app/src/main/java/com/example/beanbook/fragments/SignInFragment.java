package com.example.beanbook.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.MainActivity;
import com.example.beanbook.R;
import com.example.beanbook.activities.CreateAccountActivity;
import com.example.beanbook.dataModels.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SignInFragment extends Fragment implements View.OnClickListener {

    public static SignInFragment newInstance() { return new SignInFragment(); }

    private DatabaseReference users;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_in_fragment, container, false);

        Button signInButton = view.findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(this);

        Button createAccountButton = view.findViewById(R.id.create_account_button);
        createAccountButton.setOnClickListener(this);

        return view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get users from Firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.sign_in_button) {
            View fragmentView = getView();

            if (fragmentView != null) {
                EditText usernameET = getView().findViewById(R.id.username_edit_text);
                EditText passwordET = getView().findViewById(R.id.password_edit_text);

                if (usernameET.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), getResources().getString(R.string.must_enter_email),
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (passwordET.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), getResources().getString(R.string.must_enter_password),
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                final String username = usernameET.getText().toString();
                final String password = passwordET.getText().toString();

                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // make sure username has been registered
                        if (dataSnapshot.child(username).exists()) {
                            // pull the current user from database
                            User currUser = dataSnapshot.child(username).getValue(User.class);

                            if (currUser == null) {
                                Toast.makeText(getContext(),
                                        R.string.login_fail_error_message,
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // check for inputted password
                            if (currUser.getPassword().equals(password)) {

                                Toast.makeText(getContext(), "Login Success",
                                        Toast.LENGTH_SHORT).show();

                                // Save user to device
                                saveUser(currUser);

                                if (getActivity() == null) {
                                    return;
                                }

                                // open up the recipe list
                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("user", currUser);
                                getActivity().setResult(Activity.RESULT_OK, resultIntent);
                                getActivity().finish();

                            } else { // incorrect inputted password
                                Toast.makeText(getContext(), R.string.incorrect_password,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else  { // user does not exist
                            Toast.makeText(getContext(), "Username has not been registered",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

        } else if (v.getId() == R.id.create_account_button) {

            Intent createAccountIntent = new Intent(getContext(), CreateAccountActivity.class);
            startActivityForResult(createAccountIntent, 2);
        }
    }

    private void saveUser(User user) {

        if (getContext() != null) {
            try {
                FileOutputStream fos = getContext().openFileOutput(MainActivity.fileName, Context.MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(user);
                os.close();
                fos.close();
            } catch (IOException e) {
                Toast.makeText(getContext(),
                        R.string.login_fail_error_message,
                        Toast.LENGTH_SHORT).show();

                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(),
                    R.string.login_fail_error_message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // get the result from account creation and send it back to the recipe list
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                User newUser = (User) data.getSerializableExtra("user");

                if (getActivity() == null){
                    return;
                }

                Intent resultIntent = new Intent();
                resultIntent.putExtra("user", newUser);
                getActivity().setResult(Activity.RESULT_OK, resultIntent);
                getActivity().finish();
            }
        }
    }
}
