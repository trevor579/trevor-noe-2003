package com.example.beanbook.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.IngredientsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;

import java.util.ArrayList;
import java.util.Random;

public class TagsFragment extends Fragment implements View.OnClickListener {

    private ArrayAdapter<String> adapter;
    private ArrayList<String> tags;

    public static TagsFragment newInstance(String title, String time, String price,
                                           String beanType, User currUser, Recipe editingRecipe) {

        Bundle args = new Bundle();

        args.putString("title", title);
        args.putString("time", time);
        args.putString("price", price);
        args.putString("beanType", beanType);
        args.putSerializable("user", currUser);
        args.putSerializable("recipe", editingRecipe);

        TagsFragment fragment = new TagsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tags_fragmemt, container, false);

        tags = new ArrayList<>();

        final Button addButton = view.findViewById(R.id.add_button);
        addButton.setOnClickListener(this);

        // Handle the keyboard "enter" button as adding to list
        final EditText tagsText = view.findViewById(R.id.tags_edit_text);
        tagsText.setHint(randomHint());
        tagsText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (tagsText.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), getResources().getString(R.string.add_tag), Toast.LENGTH_SHORT).show();
                    return false;
                }

                String newTag = tagsText.getText().toString();

                tags.add(newTag);
                adapter.notifyDataSetChanged();
                tagsText.setText("");

                return true;
            }
        });

        if (getContext() != null)
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, tags);

        final ListView tagsList = view.findViewById(R.id.tags_list);
        tagsList.setAdapter(adapter);

        tagsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                // Open dialog box to let user edit the selected ingredient
                AlertDialog.Builder editBuilder = new AlertDialog.Builder(getContext());

                editBuilder.setTitle("Edit Tag");

                final EditText input = new EditText(getContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(8, 8, 8, 8);

                input.setLayoutParams(lp);
                input.setText(tags.get(position));
                editBuilder.setView(input);

                editBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (input.getText().toString().isEmpty()) {
                            Toast.makeText(getContext(),
                                    getResources().getString(R.string.cant_leave_tag_blank),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            tags.remove(position);
                            tags.add(position, input.getText().toString());
                            adapter.notifyDataSetChanged();
                        }
                    }
                });

                editBuilder.setNeutralButton("Cancel", null);
                editBuilder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tags.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                editBuilder.show();
            }
        });

        if (getArguments() != null) {
            Recipe recipe = (Recipe) getArguments().getSerializable("recipe");
            if (recipe != null) {
                if (recipe.getTags() != null) { // tags are optional so be able to handle if tags aren't present
                    for (String tag : recipe.getTags()) {
                        tags.add(tag);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
        return view;
    }

    private String randomHint() {
        Random random = new Random();
        String[] tagAutoFills = getResources().getStringArray(R.array.tags);

        return tagAutoFills[random.nextInt(tagAutoFills.length)];
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.to_ingredients_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.next_menu_item) {
            if (getView() != null) {

                if (getArguments() == null) {
                    Toast.makeText(getContext(), "Something went wrong. Try again later.", Toast.LENGTH_SHORT).show();
                    return false;
                }

                String title = getArguments().getString("title");
                String time = getArguments().getString("time");
                String price = getArguments().getString("price");
                String beanType = getArguments().getString("beanType");
                User currUser = (User) getArguments().getSerializable("user");
                Recipe editingRecipe = (Recipe) getArguments().getSerializable("recipe");

                Intent ingredientsIntent = new Intent(getContext(), IngredientsActivity.class);
                ingredientsIntent.putStringArrayListExtra("tags", tags);
                ingredientsIntent.putExtra("title", title);
                ingredientsIntent.putExtra("time", time);
                ingredientsIntent.putExtra("price", price);
                ingredientsIntent.putExtra("beanType", beanType);
                ingredientsIntent.putExtra("user", currUser);
                ingredientsIntent.putExtra("recipe", editingRecipe);
                startActivity(ingredientsIntent);

                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        if (getView() != null) {
            EditText tagsText = getView().findViewById(R.id.tags_edit_text);

            if (tagsText.getText().toString().isEmpty()) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_tag),
                        Toast.LENGTH_SHORT).show();
                return;
            }

            String newTag = tagsText.getText().toString();

            tags.add(newTag);
            adapter.notifyDataSetChanged();
            tagsText.setText("");
            tagsText.setHint(randomHint());
        }
    }
}
