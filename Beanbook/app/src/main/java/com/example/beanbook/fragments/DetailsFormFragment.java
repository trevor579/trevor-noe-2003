package com.example.beanbook.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.TagsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;

public class DetailsFormFragment extends Fragment {

    public static DetailsFormFragment newInstance(User currUser, Recipe recipe) {

        Bundle args = new Bundle();

        args.putSerializable("user", currUser);

        args.putSerializable("recipe", recipe);

        DetailsFormFragment fragment = new DetailsFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.details_form_fragment, container, false);

        EditText priceText = view.findViewById(R.id.price_text);

        // if editing, fill in form
        if (getArguments() != null) {
            Recipe recipe = (Recipe) getArguments().getSerializable("recipe");
            if (recipe != null) {

                // Set spinner to specific bean
                String[] beanTypes = getResources().getStringArray(R.array.bean_spinner_entries);
                Spinner spinner = view.findViewById(R.id.bean_spinner);

                // find the selected bean and set spinner to the same index
                for (int i = 0; i < beanTypes.length; i++) {
                    if (beanTypes[i].equals(recipe.getBeanType())) {
                        spinner.setSelection(i);
                    }
                }

                EditText titleText = view.findViewById(R.id.title_text);
                EditText timeText = view.findViewById(R.id.time_text);

                titleText.setText(recipe.getTitle());
                timeText.setText(recipe.getCookTime());
                priceText.setText(recipe.getPrice());
            }
        }

        // Go to next screen when user hits enter on keyboard while editing last text field
        priceText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                goToIngredients();

                return true;
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.to_ingredients_item, menu);
    }

    private void goToIngredients() {

        // validate input
        if (getView() != null) {
            EditText titleText = getView().findViewById(R.id.title_text);
            String title = titleText.getText().toString();

            EditText timeText = getView().findViewById(R.id.time_text);
            String time = timeText.getText().toString();

            EditText priceText = getView().findViewById(R.id.price_text);
            String price = priceText.getText().toString();

            Spinner beanSpinner = getView().findViewById(R.id.bean_spinner);
            String beanType = beanSpinner.getSelectedItem().toString();

            if (title.isEmpty() || time.isEmpty() || price.isEmpty()) {

                Toast.makeText(getContext(), R.string.fill_out_all_entries, Toast.LENGTH_SHORT).show();
                return;
            }

            User currUser = null;
            Recipe editingRecipe = null;
            if (getArguments() != null) {
                currUser = (User)getArguments().getSerializable("user");

                // if editing, set recipe
                editingRecipe = (Recipe) getArguments().getSerializable("recipe");
            }

            Intent tagsIntent = new Intent(getContext(), TagsActivity.class);
            tagsIntent.putExtra("title", title);
            tagsIntent.putExtra("time", time);
            tagsIntent.putExtra("price", price);
            tagsIntent.putExtra("beanType", beanType);
            tagsIntent.putExtra("user", currUser);

            // if editing, recipe will not be null
            tagsIntent.putExtra("recipe", editingRecipe);

            startActivity(tagsIntent);

        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.next_menu_item) {
            goToIngredients();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
