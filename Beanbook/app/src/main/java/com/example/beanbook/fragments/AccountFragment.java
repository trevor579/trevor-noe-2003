package com.example.beanbook.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.DetailsFormActivity;
import com.example.beanbook.activities.RecipeDetailsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.utilities.RecipeAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class AccountFragment extends ListFragment {

    private ArrayList<Recipe> recipes;

    public static AccountFragment newInstance(User user, ArrayList<Recipe> recipes) {

        Bundle args = new Bundle();

        args.putSerializable("user", user);
        args.putSerializable("recipes", recipes);

        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toast.makeText(getContext(), "Long press item to edit or delete, tap to view", Toast.LENGTH_LONG).show();
        return inflater.inflate(R.layout.recipe_list_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null) {
            Toast.makeText(getContext(), getResources().
                    getString(R.string.problem_retrieving_account), Toast.LENGTH_LONG).show();

            if (getActivity() != null)
                getActivity().finish();
        }

        recipes = (ArrayList<Recipe>) getArguments().getSerializable("recipes");

        RecipeAdapter adapter = new RecipeAdapter(recipes, getContext());
        setListAdapter(adapter);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (getActivity() != null)
            getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        Recipe selected = recipes.get(info.position);

        if (item.getItemId() == R.id.edit_option) {

            User currUser;
            if (getArguments() == null) {
                return false;
            }
            currUser = (User) getArguments().getSerializable("user");

            // Open Recipe Creator and pass in selected recipe
            Intent RCIntent = new Intent(getContext(), DetailsFormActivity.class);
            RCIntent.putExtra("recipe", selected);
            RCIntent.putExtra("user", currUser);
            startActivity(RCIntent);

            return true;

        } else if (item.getItemId() == R.id.delete_option) {

            // Remove the recipe from the database
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Recipes").child(selected.getTitle() + selected.getAuthor());
            databaseReference.removeValue();

            // update the local list
            recipes.remove(selected);
            RecipeAdapter recipeAdapter = (RecipeAdapter) getListAdapter();
            if (recipeAdapter != null)
                recipeAdapter.notifyDataSetChanged();

            return true;
        }

        return super.onContextItemSelected(item);
    }


    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        // Open RecipeDetails
        Recipe selectedRecipe = recipes.get(position);

        Intent detailsIntent = new Intent(getContext(), RecipeDetailsActivity.class);
        detailsIntent.putExtra("currRecipe", selectedRecipe);
        startActivity(detailsIntent);

    }
}
