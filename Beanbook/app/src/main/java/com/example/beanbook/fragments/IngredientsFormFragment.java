package com.example.beanbook.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.DirectionsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;

import java.util.ArrayList;


public class IngredientsFormFragment extends Fragment implements View.OnClickListener{

    private ArrayAdapter<String> adapter;
    private ArrayList<String> ingredients;

    public static IngredientsFormFragment newInstance(String title, String time, String price,
                                                      String beanType, User currUser,
                                                      ArrayList<String> tags, Recipe editingRecipe) {

        Bundle args = new Bundle();

        args.putString("title", title);
        args.putString("time", time);
        args.putString("price", price);
        args.putString("beanType", beanType);
        args.putSerializable("user", currUser);
        args.putStringArrayList("tags", tags);
        args.putSerializable("recipe", editingRecipe);

        IngredientsFormFragment fragment = new IngredientsFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ingredients_fragment,container,false);

        ingredients = new ArrayList<>();

        final Button addButton = view.findViewById(R.id.add_button);
        addButton.setOnClickListener(this);

        // Handle the keyboard "enter" button as adding to list
        final EditText ingredientsText = view.findViewById(R.id.ingredient_edit_text);
        ingredientsText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (ingredientsText.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_ingredient), Toast.LENGTH_SHORT).show();
                    return false;
                }

                String newIngredient = ingredientsText.getText().toString();

                ingredients.add(newIngredient);
                adapter.notifyDataSetChanged();
                ingredientsText.setText("");

                return true;
            }
        });


        if (getContext() != null)
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, ingredients);

        final ListView ingredientsList = view.findViewById(R.id.ingredients_list);
        ingredientsList.setAdapter(adapter);

        ingredientsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {

                // Open dialog box to let user edit the selected ingredient
                AlertDialog.Builder editBuilder = new AlertDialog.Builder(getContext());

                editBuilder.setTitle("Edit Ingredient");

                final EditText input = new EditText(getContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(8, 8, 8, 8);
                input.setLayoutParams(lp);

                input.setText(ingredients.get(position));

                editBuilder.setView(input);

                editBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (input.getText().toString().isEmpty()) {
                            Toast.makeText(getContext(),
                                    getResources().getString(R.string.must_fill_1_ingredient),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            ingredients.remove(position);
                            ingredients.add(position, input.getText().toString());
                            adapter.notifyDataSetChanged();
                        }
                    }
                });

                editBuilder.setNeutralButton("Cancel", null);
                editBuilder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ingredients.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                editBuilder.show();
            }
        });

        // get the ingredients user already added if user is editing
        if (getArguments() != null) {
            Recipe recipe = (Recipe) getArguments().getSerializable("recipe");

            if (recipe != null) {
                for (String ingredient: recipe.getIngredients()) {
                    ingredients.add(ingredient);
                    adapter.notifyDataSetChanged();
                }
            }
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.to_directions_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        // validate input
        if (item.getItemId() == R.id.next_menu_item) {
            if (getView() != null) {

                if (ingredients.size() == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.must_fill_1_ingredient), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (getArguments() == null) {
                    Toast.makeText(getContext(), "Something went wrong. Try again later.", Toast.LENGTH_SHORT).show();
                    return false;
                }

                String title = getArguments().getString("title");
                String time = getArguments().getString("time");
                String price = getArguments().getString("price");
                String beanType = getArguments().getString("beanType");
                User currUser = (User) getArguments().getSerializable("user");
                ArrayList<String> tags = getArguments().getStringArrayList("tags");
                Recipe editingRecipe = (Recipe) getArguments().getSerializable("recipe");

                Intent directionsIntent = new Intent(getContext(), DirectionsActivity.class);
                directionsIntent.putStringArrayListExtra("ingredients", ingredients);
                directionsIntent.putExtra("title", title);
                directionsIntent.putExtra("time", time);
                directionsIntent.putExtra("price", price);
                directionsIntent.putExtra("beanType", beanType);
                directionsIntent.putExtra("user", currUser);
                directionsIntent.putStringArrayListExtra("tags", tags);
                directionsIntent.putExtra("recipe", editingRecipe);
                startActivity(directionsIntent);

                return true;

            }
        }
        return false;
    }


    @Override
    public void onClick(View v) {

        if (getView() != null) {
            EditText ingredientEditText = getView().findViewById(R.id.ingredient_edit_text);

            if (ingredientEditText.getText().toString().isEmpty()) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_ingredient), Toast.LENGTH_SHORT).show();
                return;
            }

            String newIngredient = ingredientEditText.getText().toString();

            ingredients.add(newIngredient);
            adapter.notifyDataSetChanged();
            ingredientEditText.setText("");
        }
    }
}
