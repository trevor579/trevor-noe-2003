package com.example.beanbook.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.example.beanbook.R;
import com.example.beanbook.activities.AccountActivity;
import com.example.beanbook.activities.DetailsFormActivity;
import com.example.beanbook.activities.LearnActivity;
import com.example.beanbook.activities.RecipeDetailsActivity;
import com.example.beanbook.dataModels.Recipe;
import com.example.beanbook.dataModels.User;
import com.example.beanbook.utilities.RecipeAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class RecipeListFragment extends ListFragment implements View.OnClickListener {

    private DatabaseReference recipeRef;
    private RecipeAdapter adapter;

    private ArrayList<Recipe> recipes;

    public static RecipeListFragment newInstance(User user) {

        Bundle args = new Bundle();

        args.putSerializable("user", user);

        RecipeListFragment fragment = new RecipeListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        recipeRef = FirebaseDatabase.getInstance().getReference("Recipes");

        recipes = new ArrayList<>();

        adapter = new RecipeAdapter(recipes, getContext());
        setListAdapter(adapter);
    }

    private void updateList(final RecipeAdapter adapter) {
        recipeRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Recipe newRecipe = dataSnapshot.getValue(Recipe.class);
                recipes.add(newRecipe);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recipe_list_fragment, container, false);

        FloatingActionButton fab = view.findViewById(R.id.floating_action_button);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(this);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        // update list every time activity becomes visible.
        recipes.clear();

        updateList(adapter);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.filter_item, menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        // Filter button
        if (item.getItemId() == R.id.filter_button) {
            // Show alert dialog with list of beans for user to select from
            AlertDialog filterDialog;
            AlertDialog.Builder filterBuilder = new AlertDialog.Builder(getContext());

            final String[] beanList = getResources().getStringArray(R.array.bean_filter_entries);

            filterBuilder.setTitle("Filter By Bean").setItems(beanList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, final int which) {

                    if (which == 0) { // All Bean Types
                        RecipeAdapter adapter = new RecipeAdapter(recipes, getContext());
                        setListAdapter(adapter);
                        return;
                    }

                    ArrayList<Recipe> filteredRecipes = new ArrayList<>();

                    for (Recipe recipe : recipes) {
                        if (recipe.getBeanType().equals(beanList[which])) {
                            filteredRecipes.add(recipe);
                        }
                    }

                    RecipeAdapter adapter = new RecipeAdapter(filteredRecipes, getContext());
                    setListAdapter(adapter);
                }
            });

            filterDialog = filterBuilder.create();
            filterDialog.show();

            return true;
        }

        // Your Recipes button
        else if (item.getItemId() == R.id.account_button) {

            if (getArguments() == null) {
                return false;
            }

            User currUser = (User) getArguments().getSerializable("user");

            ArrayList<Recipe> filteredRecipes = new ArrayList<>();

            for (Recipe recipe : recipes) {
                if (currUser != null && recipe.getAuthor().equals(currUser.getUsername())) {
                    filteredRecipes.add(recipe);
                }
            }

            // Go to account screen
            Intent accountIntent = new Intent(getContext(), AccountActivity.class);
            accountIntent.putExtra("user", currUser);
            accountIntent.putExtra("recipes", filteredRecipes);
            startActivity(accountIntent);

            return true;
        }

        else if (item.getItemId() == R.id.learn_button) {
            // Go to learn screen

            Intent learnIntent = new Intent(getContext(), LearnActivity.class);
            startActivity(learnIntent);
        }

        else if (item.getItemId() == R.id.create_recipe_button) {

            onClick(null);
        }

        return false;
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        User currUser = null;

        if (getArguments() != null) {
            currUser = (User) getArguments().getSerializable("user");
        }

        // Open RecipeDetails
        Recipe selectedRecipe = recipes.get(position);

        Intent detailsIntent = new Intent(getContext(), RecipeDetailsActivity.class);
        detailsIntent.putExtra("currRecipe", selectedRecipe);
        detailsIntent.putExtra("user", currUser);
        startActivity(detailsIntent);
    }


    @Override
    public void onClick(View v) {

        User currUser = null;

        if (getArguments() != null) {
            currUser = (User) getArguments().getSerializable("user");
        }

        Intent recipeIntent = new Intent(getContext(), DetailsFormActivity.class);
        // Send current User
        recipeIntent.putExtra("user", currUser);
        startActivity(recipeIntent);
    }
}
