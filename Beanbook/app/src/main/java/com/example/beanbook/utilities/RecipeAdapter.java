package com.example.beanbook.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.beanbook.R;
import com.example.beanbook.dataModels.Recipe;

import java.util.ArrayList;

public class RecipeAdapter extends BaseAdapter {

    private static final int ID_CONSTANT = 0x0100010;

    private final ArrayList<Recipe> recipes;
    private final Context context;

    public RecipeAdapter(ArrayList<Recipe> recipes, Context context) {
        this.recipes = recipes;
        this.context = context;
    }


    @Override
    public int getCount() {
        if (recipes != null) {
            return recipes.size();
        } else {
            return 0;
        }
    }

    @Override
    public Recipe getItem(int position) {
        return recipes.get(position);
    }

    @Override
    public long getItemId(int position) {

        if(recipes != null && position >= 0 && position < recipes.size()) {
            return ID_CONSTANT + position;
        } else {
            return 0;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.recipe_item_layout, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.titleText = convertView.findViewById(R.id.title_text);
            viewHolder.authorText = convertView.findViewById(R.id.author_text);
            viewHolder.timeText = convertView.findViewById(R.id.time_text);
            viewHolder.tagsText = convertView.findViewById(R.id.tags_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Recipe recipe = getItem(position);

        if (recipe != null) {
            viewHolder.titleText.setText(recipe.getTitle());

            String author = "By: " + recipe.getAuthor();
            viewHolder.authorText.setText(author);

            String time = "Time: " + recipe.getCookTime() + " minutes";
            viewHolder.timeText.setText(time);

            if (recipe.getTags() != null) {
                StringBuilder builder = new StringBuilder();
                if (recipe.getTags().size() > 1) {
                    int counter = 0;
                    for (String tag : recipe.getTags()) {
                        builder.append(tag);
                        if (counter + 1 != recipe.getTags().size())
                            builder.append(", ");

                        counter++;
                    }
                } else {
                    builder.append(recipe.getTags().get(0));
                }
                viewHolder.tagsText.setText(builder.toString());
           }
        }

        return convertView;

    }

    private static class ViewHolder {
        TextView titleText, authorText, timeText, tagsText;
    }

}
